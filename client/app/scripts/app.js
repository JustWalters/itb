'use strict';

/**
 * @ngdoc overview
 * @name wellnessApp
 * @description
 * # wellnessApp
 *
 * Main module of the application.
 */
angular
	.module('wellnessApp', [
		'ngAnimate',
		'ngAria',
		'ngMessages',
		'ngResource',
		'ngRoute',
		'ngSanitize',
		'utils',
		'UserApp'
	])
	.config(function ($routeProvider) {
		$routeProvider
		.when('/', {
				templateUrl: 'views/main.html',
				controller: 'MainCtrl',
				controllerAs: 'main'
			})
			.when('/wellness', {
				templateUrl: 'views/conversation.html',
				controller: 'ConversationCtrl',
				public: true
			})
			.when('/login', {
				templateUrl: 'partials/login.html',
				login: true
			})
			.when('/signup', {
				templateUrl: 'partials/signup.html',
				public: true
			})
			.when('/workshops', {
				templateUrl: 'views/dashboard.html',
				controller: 'DashboardCtrl'
			})
			.when('/learning', {
				templateUrl: 'views/dashboard.html',
				controller: 'DashboardCtrl'
			})
			.when('/learning/:id', {
				templateUrl: 'views/lesson.html',
				controller: 'LessonCtrl'
			})
			.when('/splash', {
				templateUrl: 'views/splash.html',
				public: true
			})
			.when('/verify-email', {
				templateUrl: 'partials/verify-email.html',
				verify_email: true
			})
			.otherwise({
				redirectTo: '/'
			});
	})
	.run(function(user){
		user.init({appId: '56e09d8349e5e'});
	});
