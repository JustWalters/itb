'use strict';

/**
 * @ngdoc function
 * @name wellnessApp.controller:ConversationCtrl
 * @description
 * # ConversationCtrl
 * Controller of the wellnessApp
 */
angular.module('wellnessApp')
	.controller('ConversationCtrl', function ($scope, $window, ConvoQuestion) {
		$scope.activeCat = 'Getting to Know You';
		$scope.steps = [
			{name: 'Getting to Know You', img: 'icon/intro'},
			{name: 'Goals and Obligations', img: 'icon/goals'},
			{name: 'Financial Concerns', img: 'icon/concerns'},
			{name: 'Employment & Income', img: 'icon/employment'},
			{name: 'Expenses', img: 'icon/expenses'},
			{name: 'Retirement Planning', img: 'icon/retirement'},
			{name: 'Net Worth (Assets & Liabilites)', img: 'icon/networth'},
			{name: 'College Planning', img: 'icon/college'},
			{name: 'Insurance', img: 'icon/insurance'},
			{name: 'Estate Planning', img: 'icon/estate'}
		];

		$scope.questions = ConvoQuestion.byCategory($scope.activeCat);

		$scope.prevValid = function prevValid() {
			if (this.$first) return true;

			for (var i = 0; i < this.$index; i++) {
				if ($scope.questions[i].unanswered) return false;
			}

			return true;
		};

		$scope.nextCat = function nextCat() {
			var cats = $scope.steps.map(function(x){ return x.name; }),
					curCat = $scope.activeCat,
					numOfCats = cats.length,
					catIndex = cats.indexOf(curCat),
					curQuestions = $scope.questions.filter(function(x){ return x.category === curCat; }),
					allAnswered = curQuestions.every(function(x){ return x.answered; });

			if (allAnswered) {
				if (catIndex < numOfCats - 1) {
					$scope.activeCat = cats[catIndex + 1];
					$scope.questions = ConvoQuestion.byCategory($scope.activeCat);
				} else $window.location.href = '/';
			} else {
				alert('Please answer all of the questions.');
			}
		};
	});

