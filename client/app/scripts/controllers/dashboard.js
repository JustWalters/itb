'use strict';

/**
 * @ngdoc function
 * @name wellnessApp.controller:DashboardCtrl
 * @description
 * # DashboardCtrl
 * Controller of the wellnessApp
 */
angular.module('wellnessApp')
	.controller('DashboardCtrl', function ($scope, $location) {
		$scope.page = '';
		if ($location.$$path.indexOf('workshops') > -1) {
			$scope.includePage = 'views/workshop.html';
			$scope.activeCat = 'dashboard';
		}	else {
			$scope.includePage = 'views/learning.html';
			$scope.activeCat = 'courses';
		}

	});
