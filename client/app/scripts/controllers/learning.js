'use strict';

/**
 * @ngdoc function
 * @name wellnessApp.controller:LearningCtrl
 * @description
 * # LearningCtrl
 * Controller of the wellnessApp
 */
angular.module('wellnessApp')
	.controller('LearningCtrl', function ($scope) {
		var ipsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor ' +
		'incididunt ut labore et dolore magna aliqua.';
		$scope.recommended = {
			name: 'LI',
			description: ipsum,
			img: 'images/classes/401k-sign.jpg'
		};
		$scope.classes = [];
		for (var i = 0; i < 8; i++) {
			$scope.classes.push({
				id: 'foo',
				name: 'Emergency Saving',
				description: ipsum,
				img: 'images/classes/401k-sign.jpg'
			});
		}

		$scope.popular = {
			name: 'Road Trip',
			subtitle: 'Planning Your Life\'s Financial Roadmap',
			description: ipsum,
			img: 'images/classes/roadtrip.png'
		};
	});
