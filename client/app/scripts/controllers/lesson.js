'use strict';

/**
 * @ngdoc function
 * @name wellnessApp.controller:LessonCtrl
 * @description
 * # LessonCtrl
 * Controller of the wellnessApp
 */
angular.module('wellnessApp')
	.controller('LessonCtrl', function ($scope, $routeParams) {
		console.log($scope, $routeParams);
	});
