'use strict';

/**
 * @ngdoc function
 * @name wellnessApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the wellnessApp
 */
angular.module('wellnessApp')
	.controller('MainCtrl', function ($scope) {
		var ipsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor ' +
		'incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud ' +
		'exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor ' +
		'in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur ' +
		'sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est ' +
		'laborum';
		var classes = [{
			name: '401k and Retirement',
			description: ipsum.substring(0,60),
			img: 'images/classes/401k.jpg'
		},
		{
			name: 'Another One',
			description: ipsum.substring(0,60),
			img: 'images/classes/piggy-bank.jpg'
		}];
		$scope.categories = [
			{
				name: 'Debt',
				score: 56,
				explanation: 'This score is based on all the debt you have and how ' +
										 'it impacts your financial health.',
				longExplanation: ipsum,
				classes: classes
			},
			{
				name: 'General Savings',
				score: 56,
				explanation: 'Life happens and an emergency savings account protects you.',
				longExplanation: ipsum,
				classes: classes
			},
			{
				name: 'Emergency Savings',
				score: 56,
				explanation: 'Life happens and an emergency savings account protects you.',
				longExplanation: ipsum,
				classes: classes
			}
		];
	});
