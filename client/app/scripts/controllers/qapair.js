'use strict';

/**
 * @ngdoc function
 * @name wellnessApp.controller:QapairCtrl
 * @description
 * # QapairCtrl
 * Controller of the wellnessApp
 */
angular.module('wellnessApp')
	.controller('QapairCtrl', function ($scope) {
			var flat = [];
			if ($scope.q.responses) {
				flat = $scope.q.responses.reduce(function(a, b){
					if (angular.isArray(b)) {
						return a.concat(b);
					} else {
						return a.push(b), a; // return updated array
					}
				}, []);
			} else if ($scope.q.opts) {
				flat = $scope.q.opts.map(function(x) { return x.val; });
			}

			$scope.q.errors = [];
			$scope.updateStatus = function updateStatus() {
				var answer = $scope.q.answer;
				if (!answer) {
					$scope.q.answered = false;
					$scope.q.unanswered = !$scope.q.answered;
					return;
				}
				$scope.readonly = true;

				if (answer && validateAnswer(answer, flat)) {
					$scope.q.answer = ObjToArrayOrString(answer, 'label');
					$scope.q.answered = true;
				} else {
					$scope.q.answered = false;
					if (!!answer) { //attempted answer, not premature submission
						pushError(answer, flat);
					}
				}
				$scope.q.unanswered = !$scope.q.answered;
			};

			$scope.updateStatusFromError = function updateStatusFromError(errAnswer) {
				if (errAnswer && validateAnswer(errAnswer, flat)) {
					$scope.q.answer = ObjToArrayOrString(errAnswer, 'label');
					$scope.q.answered = true;
				} else {
					$scope.q.answered = false;
					if (!!errAnswer) {
						pushError(errAnswer, flat);
					}
				}
				$scope.q.unanswered = !$scope.q.answered;
			};

			$scope.checkEnterPressed = function checkEnterPressed($event) {
				var keyCode = $event.which || $event.keyCode;
					if (keyCode === 13) { // enter key
							$scope.updateStatus();
					}
			};

			$scope.updateStatus();

			function ObjToArrayOrString(answer, prop) {
				if (!answer || angular.isString(answer)) return answer;
				if (angular.isObject(answer)) {
					if (answer[prop]) {
						// answer is {val: '', label: ''}
						return answer[prop];
					}
					// else is essentially an array of objects
					var res = [];
					for(var idx in answer) {
						if (answer.hasOwnProperty(idx)) {
							res.push(answer[idx][prop]);
						}
					}
					return res;
				}
				return answer;
			}

			function validateAnswer(given, acceptable) {
				given = ObjToArrayOrString(given, 'val');
				// given multiple answers (checkboxes)
				if (angular.isArray(given)) {
					return given.every(function(ans) {
						return acceptable.indexOf(ans.toLowerCase()) > -1;
					});
       } else { // given a single answer
         var accepted = acceptable.filter(function(a) {
           return a.indexOf(given.toLowerCase()) === 0;
         });
         if (accepted.length < 1) { return false; }
         if (accepted.length === 1) { return true; }
         // ambiguous
         // TODO: find closest match, confirm
         return false;
       }
			}

			function pushError(given, acceptable) {
				var possibleErrs = ["Sorry, I don't know what " + given + ' means.',
					'Could you rephrase? I do not understand ' + given + '.',
					'Please provide an answer exactly as I gave it. I am not sure what you mean by '+
					given + '.'],
				err = possibleErrs[Math.floor(Math.random() * possibleErrs.length)];
				given = ObjToArrayOrString(given, 'label');
				if (angular.isArray(given)) {
					given = given.join(', ');
				}
				$scope.q.errors.push({
					category: 'error',
					isError: true,
					text: err,
					responses: acceptable
				});
			}
	});
