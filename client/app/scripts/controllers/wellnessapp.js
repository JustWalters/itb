'use strict';

/**
 * @ngdoc function
 * @name wellnessApp.controller:WellnessAppCtrl
 * @description
 * # WellnessAppCtrl
 * Controller of the wellnessApp
 */
angular.module('wellnessApp')
	.controller('WellnessAppCtrl', function ($scope, $location) {
		$scope.hideNavLinks = hideNavLinks;

		function hideNavLinks() {
			var path = $location.path(),
					noNavRoutes = ['/login', '/signup', '/splash', '/verify-email'];
			return noNavRoutes.indexOf(path) > -1;
		}
	});
