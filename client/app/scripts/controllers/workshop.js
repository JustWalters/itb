'use strict';

/**
 * @ngdoc function
 * @name wellnessApp.controller:WorkshopCtrl
 * @description
 * # WorkshopCtrl
 * Controller of the wellnessApp
 */
angular.module('wellnessApp')
	.controller('WorkshopCtrl', function ($scope) {
		var ipsum = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor ' +
		'incididunt ut labore et dolore magna aliqua.';

		$scope.lessons = [];

		for (var i = 0; i < 3; i++) {
			$scope.lessons.push({
				name: 'Lorem Ipsum',
				description: ipsum,
				progress: 60
			});
		}
		var jan5 = new Date(1483635600000);
		$scope.registered = [
		{
			name: 'Your Longest Coffee Break',
			img: 'images/classes/401k.jpg',
			date: jan5,
			time: jan5,
			location: 'Room 602'
		}];

		$scope.upcoming = [{
			name: 'Your Longest Coffee Break',
			date: jan5,
			time: jan5,
			location: 'Room 602'
		},{
			name: 'Your Longest Coffee Break',
			date: jan5,
			time: jan5,
			location: 'Room 602'
		}];

	});
