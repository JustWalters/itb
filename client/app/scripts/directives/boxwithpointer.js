'use strict';

/**
 * @ngdoc directive
 * @name wellnessApp.directive:boxWithPointer
 * @description
 * # boxWithPointer
 */
angular.module('wellnessApp')
	.directive('boxWithPointer', function () {
		return {
			templateUrl: 'views/directives/boxwithpointer.html',
			restrict: 'E',
			scope: {
				text: '@',
				active: '@',
				pointer: '@'
			},
			controller: function() {
				// console.log(753, $scope);
			}
		};
	});
