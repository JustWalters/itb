'use strict';

/**
 * @ngdoc directive
 * @name wellnessApp.directive:errorPair
 * @description
 * # errorPair
 */
angular.module('wellnessApp')
	.directive('errorPair', function () {
		return {
			templateUrl: 'views/directives/errorpair.html',
			restrict: 'E',
			controller: function($scope) {
				$scope.readonly = false;
				$scope.updateErrorStatus = function() {
					var answer = $scope.eanswer;
					if (!answer) {
						$scope.answered = false;
						$scope.unanswered = !$scope.answered;
						return;
					}

					$scope.readonly = true;

					if (answer &&
						$scope.$parent.e.responses.indexOf(
							answer.toLocaleLowerCase()) > -1) {
						$scope.answered = true;
					} else {
						$scope.answered = false;
						if (!!answer) {
						}
					}

					$scope.unanswered = !$scope.answered;
					$scope.$parent.$parent.updateStatusFromError(answer);
				};

				$scope.checkEnterPressed = function checkEnterPressed($event) {
					var keyCode = $event.which || $event.keyCode;
						if (keyCode === 13) { // enter key
								$scope.updateErrorStatus();
						}
				};
			}
		};
	});
