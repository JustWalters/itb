'use strict';

/**
 * @ngdoc directive
 * @name wellnessApp.directive:navbar
 * @description
 * # navbar
 */
angular.module('wellnessApp')
	.directive('navbar', function () {
		return {
			templateUrl: 'views/directives/navbar.html',
			restrict: 'E',
			scope: {
				hideNavLinks: '<',
				showSignIn: '<'
			},
			controller: 'NavbarCtrl'
		};
	});
