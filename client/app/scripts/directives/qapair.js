'use strict';

/**
 * @ngdoc directive
 * @name wellnessApp.directive:qaPair
 * @description
 * # qaPair
 */
angular.module('wellnessApp')
	.directive('qaPair', function () {
		return {
			templateUrl: 'views/directives/qapair.html',
			restrict: 'E',
			controller: 'QapairCtrl'
		};
	});
