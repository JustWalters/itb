'use strict';

/**
 * @ngdoc filter
 * @name wellnessApp.filter:joinList
 * @function
 * @description
 * # joinList
 * Filter in the wellnessApp.
 */
angular.module('wellnessApp')
	.filter('joinList', function () {
		return function j(input) {
			if (angular.isString(input)) return input;
			if (angular.isArray(input)) {
				var len = input.length;
				if (len === 1) return input[0];
				if (len === 2) return input[0] + ' and ' + input[1];

				var res = '';
				input.forEach(function(el, i) {
					if (i < len - 1) {
						res += input[i] + ', ';
					} else {
						res += 'and ' + input[i];
					}
				});
				return res;
			}
			return input;
		};
	});
