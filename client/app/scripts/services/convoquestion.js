'use strict';

/**
 * @ngdoc service
 * @name wellnessApp.ConvoQuestion
 * @description
 * # ConvoQuestion
 * Service in the wellnessApp.
 */
angular.module('wellnessApp')
	.service('ConvoQuestion', function (user) {
		// AngularJS will instantiate a singleton by calling "new" on this function
		var yes = ['yes', 'yeah', 'yep', 'y'],
				no = ['no', 'nope', 'n'],
				curUser = user.current,
				questions = [
					{category: 'Getting to Know You',
					text: 'Hello, ' + curUser.first_name + ", we'll need some basic info... " +
					'Do you have this info ready?',
					responses: [yes, no]},
					// {category: 'Getting to Know You',
					// text: 'Check some boxes',
					// type: 'check',
					// opts: [{val: 'blue', label: 'Blue'}, {val: 'red', label: 'Red'}]
					// },
					// {category: 'Getting to Know You',
					// text: 'select one box',
					// type: 'dropdown',
					// opts: [{val: 'blue', label: 'Blue'}, {val: 'red', label: 'Red'}]
					// },
					// {category: 'Getting to Know You',
					// text: 'Check one box',
					// type: 'radio',
					// opts: [{val: 'blue', label: 'Blue'}, {val: 'red', label: 'Red'}]
					// },
					{category: 'Getting to Know You',
					text: "Ok. Let's begin. What's your marital status? " +
					'Single, Married, Domestic Partnership, Divorced, or Widowed',
					responses: ['single', 'married', 'domestic partnership',
											'divorced', 'widowed']},
					{category: 'Getting to Know You', text: 'Ok. What\'s your date of birth?', responses: [yes]},
					{category: 'Getting to Know You', text: 'Ok. Do you have any children or dependents?', responses: [yes]},
					{category: 'Getting to Know You', text: 'What are their names and ages?', responses: [yes]},

					{category: 'Goals and Obligations', text: 'Great. Now let\'s discuss some of the goals you have in the near future? What are your major goals in the next 3 years?', responses: [yes]},
					{category: 'Goals and Obligations', text: 'Ok. That\'s great. Do you have any major financial obligations coming up in the next 2 years?', responses: [yes]},
					{category: 'Goals and Obligations', text: 'And do you have any major financial obligations in the next 5 years?', responses: [yes]},

					{category: 'Financial Concerns', text: 'Below is a list of concerns most people say they have about their finances. Please check off any and all that you feel applies to you.', responses: [yes]},

					{category: 'Employment & Income', text: 'Great. Let\'s talk about what you do. Are you a full-time employee, full-time student, part-time employee, part-time student, self-employed, a stay at home parent or are you looking for a job (unemployed)?', responses: [yes]},
					{category: 'Employment & Income', text: 'Ok. How much do you make per year? If you\'re hourly, you can multiply your hourly rate by 2080 to get your annual salary.', responses: [yes]},
					{category: 'Employment & Income', text: 'And what is your total monthly take home?', responses: [yes]},
					{category: 'Employment & Income', text: 'Do you have any other income? Examples of other types of income are bonuses, alimony, investment income, rental income, gifts received and refunds or reimbursements.', responses: [yes]},
					{category: 'Employment & Income', text: 'Ok.', responses: [yes]},

					{category: 'Expenses', text: 'Now let\'s talk about your monthly expenses not including debt or savings. There are two ways to provide your monthly expenses. You can link your accounts that you use to do your spending by choosing among the banks listed or you can enter it manually. How do you want to provide your expenses?', responses: [yes],},
					{category: 'Expenses', text: 'Ok, in order for us to link your accounts we\'ll use a secure bank-level encrypted process. Please select the bank you use to do your spending.', responses: [yes],},
					{category: 'Expenses', text: 'Great, it looks like we were able to connect to your account. Do you have any other accounts you use to do your spending?\'', responses: [yes],},

					{category: 'Retirement Planning', text: 'Ok. Let\'s talk about planning for your retirement. At what age do you expect to retire?', responses: [yes],},
					{category: 'Retirement Planning', text: 'Ok, let\'s talk about how much money you\'ll need annually after you retire. Given your salary, we normally recommend saving enough to live off at least 70% of your salary in today\'s dollars. For you that would be $70,000 per year. Do you think you\'ll need more than that in retirement?', responses: [yes],},
					{category: 'Retirement Planning', text: 'Do you have a retirement account through work?', responses: [yes],},
					{category: 'Retirement Planning', text: 'Take a look at the list of the different retirement plan types and check off any that you currently have at work.', responses: [yes],},
					{category: 'Retirement Planning', text: 'Let\'s start with the 401(k). How much do you contribute to it every month?', responses: [yes],},
					{category: 'Retirement Planning', text: 'Does your company match your contributions?', responses: [yes],},
					{category: 'Retirement Planning', text: 'Below is a list of the normal types of employer contributions. Please check one.', responses: [yes],},

					// {category: 'Net Worth (Assets & Liabilites)', text: '', responses: [],},
					// {category: 'College Planning', text: '', responses: [],},
					// {category: 'Insurance', text: '', responses: [],},
					// {category: 'Estate Planning', text: '', responses: [],},
				];


		this.byCategory = function byCategory(cat) {
			return questions.filter(function(x) { return x.category === cat; });
		};
	});
