'use strict';

describe('Controller: ConverstionCtrl', function () {

  // load the controller's module
  beforeEach(module('wellnessApp'));

  var ConverstionCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    ConverstionCtrl = $controller('ConverstionCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(ConverstionCtrl.awesomeThings.length).toBe(3);
  });
});
