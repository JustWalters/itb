'use strict';

describe('Controller: QapairCtrl', function () {

  // load the controller's module
  beforeEach(module('wellnessApp'));

  var QapairCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    QapairCtrl = $controller('QapairCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(QapairCtrl.awesomeThings.length).toBe(3);
  });
});
