'use strict';

describe('Controller: WellnessappctrlCtrl', function () {

  // load the controller's module
  beforeEach(module('wellnessApp'));

  var WellnessappctrlCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    WellnessappctrlCtrl = $controller('WellnessappctrlCtrl', {
      $scope: scope
      // place here mocked dependencies
    });
  }));

  it('should attach a list of awesomeThings to the scope', function () {
    expect(WellnessappctrlCtrl.awesomeThings.length).toBe(3);
  });
});
