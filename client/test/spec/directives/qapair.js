'use strict';

describe('Directive: qaPair', function () {

  // load the directive's module
  beforeEach(module('wellnessApp'));

  var element,
    scope;

  beforeEach(inject(function ($rootScope) {
    scope = $rootScope.$new();
  }));

  it('should make hidden element visible', inject(function ($compile) {
    element = angular.element('<qa-pair></qa-pair>');
    element = $compile(element)(scope);
    expect(element.text()).toBe('this is the qaPair directive');
  }));
});
