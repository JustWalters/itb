'use strict';

describe('Filter: joinList', function () {

  // load the filter's module
  beforeEach(module('wellnessApp'));

  // initialize a new instance of the filter before each test
  var joinList;
  beforeEach(inject(function ($filter) {
    joinList = $filter('joinList');
  }));

  it('should return the input prefixed with "joinList filter:"', function () {
    var text = 'angularjs';
    expect(joinList(text)).toBe('joinList filter: ' + text);
  });

});
