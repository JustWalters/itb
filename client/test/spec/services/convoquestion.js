'use strict';

describe('Service: ConvoQuestion', function () {

  // load the service's module
  beforeEach(module('wellnessApp'));

  // instantiate service
  var ConvoQuestion;
  beforeEach(inject(function (_ConvoQuestion_) {
    ConvoQuestion = _ConvoQuestion_;
  }));

  it('should do something', function () {
    expect(!!ConvoQuestion).toBe(true);
  });

});
